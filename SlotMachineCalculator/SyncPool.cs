namespace SlotMachineCalculator;

public class SyncPool<T> where T : class, new()
{
    private object syncObject = new();
    private List<T> list = new();
    public IEnumerable<T> GetAll() => list;
    
    public T GetInstance()
    {
        T? result = null; 
        lock (syncObject)
        {
            var cnt = list.Count-1;
            if (cnt >= 0)
            {
                result = list[cnt];
                list.RemoveAt(cnt);
            }
        }
        return result ?? new T();
    }

    public void ReturnInstance(T value)
    {
        lock (syncObject)
        {
            list.Add(value);
        }
    }

}