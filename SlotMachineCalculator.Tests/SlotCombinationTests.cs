using SlotMachineCalculator;

namespace SlotMachineCalculator.Tests;

public class SlotCombinationTests
{
    [Theory]
    [InlineData(0,0,0,0,0,0,0,0,0,3)]
    [InlineData(0,0,0,0,0,0,0,0,0,4)]
    [InlineData(0,0,0,0,0,0,0,0,0,5)]
    [InlineData(0,0,0,0,0,0,1,1,1,0)]
    [InlineData(1,1,1,1,1,1,0,0,0,0)]
    [InlineData(1,2,3,4,5,6,7,8,9,4)]
    [InlineData(9,8,7,6,5,4,3,2,1,3)]
    [InlineData(25,25,25,25,25,25,25,25,25,5)]
    [InlineData(17,18,19,20,21,22,23,24,25,3)]
    public void SlotCombinationShouldGenerateAndUnpackCombinationToLongId(
        int l1, int l2, int l3, int l4, int l5, int l6, int l7, int l8, int l9, 
        int spinCount
    )
    {
        var id = SlotCombination.GetCombinationId(l1, l2, l3, l4, l5, l6, l7, l8, l9, spinCount);
        var actual = SlotCombination.ExtractFromId(id);
        Assert.Equal(l1, actual.LineCombinations[0]);
        Assert.Equal(l2, actual.LineCombinations[1]);
        Assert.Equal(l3, actual.LineCombinations[2]);
        Assert.Equal(l4, actual.LineCombinations[3]);
        Assert.Equal(l5, actual.LineCombinations[4]);
        Assert.Equal(l6, actual.LineCombinations[5]);
        Assert.Equal(l7, actual.LineCombinations[6]);
        Assert.Equal(l8, actual.LineCombinations[7]);
        Assert.Equal(l9, actual.LineCombinations[8]);
        Assert.Equal(spinCount, actual.SpinCount);
    }
    
    [Theory]
    [InlineData(0, 0)]
    [InlineData(1, 0)]
    [InlineData(2, 0)]
    [InlineData(3, 3)]
    [InlineData(4, 4)]
    [InlineData(5, 5)]
    [InlineData(6 ,6)]
    [InlineData(7, 7)]
    public void SlotCombinationIdShouldNotContainsSpinCountLessThen3(int spinCount, int expectedSpinCount)
    {
        var id = SlotCombination.GetCombinationId(1, 1, 1, 1, 1, 1, 1, 1, 1, spinCount);
        var actual = SlotCombination.ExtractFromId(id);
        Assert.Equal(expectedSpinCount, actual.SpinCount);
    }

    private (int, int, int, int, int) LineToIds(string line)
    {
        return (
            Consts.SignToId(line[0]),
            Consts.SignToId(line[1]),
            Consts.SignToId(line[2]),
            Consts.SignToId(line[3]),
            Consts.SignToId(line[4])
        );
    }

    private void TestLineCombination(string line, int expectedCombination)
    {
        var (id1, id2, id3, id4, id5) = LineToIds(line);
        var actual = SlotCombination.GetCombinationForLine(id1, id2, id3, id4, id5);
        Assert.Equal(expectedCombination, actual);
    }

    [Theory]
    [InlineData("AABBC", Consts.COMBINATION_NONE)]
    [InlineData("ABCDE", Consts.COMBINATION_NONE)]
    [InlineData("ASSSE", Consts.COMBINATION_NONE)]
    [InlineData("SSSSS", Consts.COMBINATION_NONE)]
    [InlineData("ABBBE", Consts.COMBINATION_NONE)]
    [InlineData("CAAAD", Consts.COMBINATION_NONE)]
    [InlineData("AABAA", Consts.COMBINATION_NONE)]
    [InlineData("ASADA", Consts.COMBINATION_NONE)]
    
    [InlineData("AAABC", Consts.COMBINATION_A + Consts.COMBINATION_3)]
    [InlineData("BBBAA", Consts.COMBINATION_B + Consts.COMBINATION_3)]
    [InlineData("SSCCC", Consts.COMBINATION_C + Consts.COMBINATION_3)]
    [InlineData("FADDD", Consts.COMBINATION_D + Consts.COMBINATION_3)]
    [InlineData("EEEHH", Consts.COMBINATION_E + Consts.COMBINATION_3)]
    [InlineData("FFFDG", Consts.COMBINATION_F + Consts.COMBINATION_3)]
    [InlineData("HSGGG", Consts.COMBINATION_G + Consts.COMBINATION_3)]
    [InlineData("BBHHH", Consts.COMBINATION_H + Consts.COMBINATION_3)]
    
    [InlineData("AAAAC", Consts.COMBINATION_A + Consts.COMBINATION_4)]
    [InlineData("BBBBA", Consts.COMBINATION_B + Consts.COMBINATION_4)]
    [InlineData("ACCCC", Consts.COMBINATION_C + Consts.COMBINATION_4)]
    [InlineData("DDDDF", Consts.COMBINATION_D + Consts.COMBINATION_4)]
    [InlineData("EEEED", Consts.COMBINATION_E + Consts.COMBINATION_4)]
    [InlineData("EFFFF", Consts.COMBINATION_F + Consts.COMBINATION_4)]
    [InlineData("GGGGS", Consts.COMBINATION_G + Consts.COMBINATION_4)]
    [InlineData("SHHHH", Consts.COMBINATION_H + Consts.COMBINATION_4)]
    
    [InlineData("AAAAA", Consts.COMBINATION_A + Consts.COMBINATION_5)]
    [InlineData("BBBBB", Consts.COMBINATION_B + Consts.COMBINATION_5)]
    [InlineData("CCCCC", Consts.COMBINATION_C + Consts.COMBINATION_5)]
    [InlineData("DDDDD", Consts.COMBINATION_D + Consts.COMBINATION_5)]
    [InlineData("EEEEE", Consts.COMBINATION_E + Consts.COMBINATION_5)]
    [InlineData("FFFFF", Consts.COMBINATION_F + Consts.COMBINATION_5)]
    [InlineData("GGGGG", Consts.COMBINATION_G + Consts.COMBINATION_5)]
    [InlineData("HHHHH", Consts.COMBINATION_H + Consts.COMBINATION_5)]
    public void GetCombinationForLineShouldReturnRightCombinationWhenNoWild(string line, int expectedCombination)
    {
        TestLineCombination(line, expectedCombination);
    }
    
    [Theory]
    [InlineData("AWBBC", Consts.COMBINATION_NONE)]
    [InlineData("ABWDE", Consts.COMBINATION_NONE)]
    [InlineData("ASSWS", Consts.COMBINATION_NONE)]
    [InlineData("SSWSS", Consts.COMBINATION_NONE)]
    [InlineData("ABWBE", Consts.COMBINATION_NONE)]
    [InlineData("CAAWD", Consts.COMBINATION_NONE)]
    
    [InlineData("AWABA", Consts.COMBINATION_A + Consts.COMBINATION_3)]
    [InlineData("BBWAB", Consts.COMBINATION_B + Consts.COMBINATION_3)]
    [InlineData("ASCWC", Consts.COMBINATION_C + Consts.COMBINATION_3)]
    [InlineData("SSWDD", Consts.COMBINATION_D + Consts.COMBINATION_3)]
    [InlineData("EWEDE", Consts.COMBINATION_E + Consts.COMBINATION_3)]
    [InlineData("FFWDF", Consts.COMBINATION_F + Consts.COMBINATION_3)]
    [InlineData("GWGAC", Consts.COMBINATION_G + Consts.COMBINATION_3)]
    [InlineData("HHWSS", Consts.COMBINATION_H + Consts.COMBINATION_3)]
    
    [InlineData("GGWHH", Consts.COMBINATION_G + Consts.COMBINATION_3)] // G3 > H3
    [InlineData("AAWCC", Consts.COMBINATION_A + Consts.COMBINATION_3)] // A3 > C3
    
    [InlineData("AAAWC", Consts.COMBINATION_A + Consts.COMBINATION_4)]
    [InlineData("BBWBA", Consts.COMBINATION_B + Consts.COMBINATION_4)]
    [InlineData("AWCCC", Consts.COMBINATION_C + Consts.COMBINATION_4)]
    [InlineData("DWDDC", Consts.COMBINATION_D + Consts.COMBINATION_4)]
    [InlineData("EEWED", Consts.COMBINATION_E + Consts.COMBINATION_4)]
    [InlineData("EFFWF", Consts.COMBINATION_F + Consts.COMBINATION_4)]
    [InlineData("GGWGS", Consts.COMBINATION_G + Consts.COMBINATION_4)]
    [InlineData("SHHWH", Consts.COMBINATION_H + Consts.COMBINATION_4)]
    
    [InlineData("AWAAA", Consts.COMBINATION_A + Consts.COMBINATION_5)]
    [InlineData("BBWBB", Consts.COMBINATION_B + Consts.COMBINATION_5)]
    [InlineData("CCCWC", Consts.COMBINATION_C + Consts.COMBINATION_5)]
    [InlineData("DWDDD", Consts.COMBINATION_D + Consts.COMBINATION_5)]
    [InlineData("EEEWE", Consts.COMBINATION_E + Consts.COMBINATION_5)]
    [InlineData("FFWFF", Consts.COMBINATION_F + Consts.COMBINATION_5)]
    [InlineData("GWGGG", Consts.COMBINATION_G + Consts.COMBINATION_5)]
    [InlineData("HHHWH", Consts.COMBINATION_H + Consts.COMBINATION_5)]
    public void GetCombinationForLineShouldReturnRightCombinationWith1Wild(string line, int expectedCombination)
    {
        TestLineCombination(line, expectedCombination);
    }

    [Theory]
    [InlineData("SAWWS", Consts.COMBINATION_NONE)]
    [InlineData("SWSWS", Consts.COMBINATION_NONE)]
    [InlineData("AWSWB", Consts.COMBINATION_NONE)]
    [InlineData("EWFWG", Consts.COMBINATION_NONE)]
    
    [InlineData("AWWCC", Consts.COMBINATION_C + Consts.COMBINATION_4)] // C4 > A3
    [InlineData("AWWHH", Consts.COMBINATION_A + Consts.COMBINATION_3)] // A3 > H4
    
    [InlineData("AAWWC", Consts.COMBINATION_A + Consts.COMBINATION_4)]
    [InlineData("BWWBA", Consts.COMBINATION_B + Consts.COMBINATION_4)]
    [InlineData("FWWCC", Consts.COMBINATION_C + Consts.COMBINATION_4)]
    [InlineData("CWDWD", Consts.COMBINATION_D + Consts.COMBINATION_4)]
    [InlineData("EWWED", Consts.COMBINATION_E + Consts.COMBINATION_4)]
    [InlineData("EFWWF", Consts.COMBINATION_F + Consts.COMBINATION_4)]
    [InlineData("GWGWS", Consts.COMBINATION_G + Consts.COMBINATION_4)]
    [InlineData("HWHWA", Consts.COMBINATION_H + Consts.COMBINATION_4)]
    
    [InlineData("AWWAA", Consts.COMBINATION_A + Consts.COMBINATION_5)]
    [InlineData("BBWWB", Consts.COMBINATION_B + Consts.COMBINATION_5)]
    [InlineData("CCWWC", Consts.COMBINATION_C + Consts.COMBINATION_5)]
    [InlineData("DWWDD", Consts.COMBINATION_D + Consts.COMBINATION_5)]
    [InlineData("EWEWE", Consts.COMBINATION_E + Consts.COMBINATION_5)]
    [InlineData("FWWFF", Consts.COMBINATION_F + Consts.COMBINATION_5)]
    [InlineData("GWGWG", Consts.COMBINATION_G + Consts.COMBINATION_5)]
    [InlineData("HHWWH", Consts.COMBINATION_H + Consts.COMBINATION_5)]
    public void GetCombinationForLineShouldReturnRightCombinationWith2Wilds(string line, int expectedCombination)
    {
        TestLineCombination(line, expectedCombination);
    }

    [Theory]
    [InlineData("SWWWS", Consts.COMBINATION_NONE)]

    [InlineData("AWWWC", Consts.COMBINATION_A + Consts.COMBINATION_4)] 
    [InlineData("AWWWH", Consts.COMBINATION_A + Consts.COMBINATION_4)] 
    [InlineData("BWWWB", Consts.COMBINATION_B + Consts.COMBINATION_5)]  
    [InlineData("GWWWH", Consts.COMBINATION_G + Consts.COMBINATION_4)] 
    public void GetCombinationForLineShouldReturnRightCombinationWith3Wilds(string line, int expectedCombination)
    {
        TestLineCombination(line, expectedCombination);
    }
}