namespace SlotMachineCalculator;

public class SlotCalculator
{
    private int[] DistinctSideTriplets = Array.Empty<int>();
    private int[] DistinctMediumTriplets = Array.Empty<int>();
    private uint[] SideTripletsCounts = Array.Empty<uint>();
    private uint[] MediumTripletsCounts = Array.Empty<uint>();
    
    
    public CombinationDistribution Calculate()
    {
        (DistinctSideTriplets, SideTripletsCounts) = CreateDistinctTables(Consts.SideTriplets);
        (DistinctMediumTriplets, MediumTripletsCounts) = CreateDistinctTables(Consts.MediumTriplets);
        var results = new SyncPool<CombinationDistribution>();
        Parallel.For(0, DistinctMediumTriplets.Length, i =>
        {
            var triplet = DistinctMediumTriplets[i];
            CalculatePartitionSafe(triplet, MediumTripletsCounts[i], results);
        });
        return results.GetAll().Aggregate((result, next) => result.Merge(next));
    }

    private (int[] distinct, uint[] counts) CreateDistinctTables(int[] triplets)
    {
        var groups = triplets.GroupBy(x => x).ToList();
        var distinct = new int[groups.Count];
        var counts = new uint[groups.Count];
        for (var i = 0; i < groups.Count; i++)
        {
            distinct[i] = groups[i].Key;
            counts[i] = (uint) groups[i].Count();
        }
        return (distinct, counts);
    }

    public void CalculatePartitionSafe(int mediumTriplet, uint mediumTripletCnt,
        SyncPool<CombinationDistribution> results)
    {
        try
        {
            CalculatePartition(mediumTriplet, mediumTripletCnt, results);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
    }


    public void CalculatePartition(int mediumTriplet, uint mediumTripletCnt, SyncPool<CombinationDistribution> results)
    {
        var distrib = results.GetInstance();
        var (s13, s23, s33) = mediumTriplet.ExtractTriplet();
        for (var i1 = 0; i1 < DistinctSideTriplets.Length; i1++)
        {
            var col1 = DistinctSideTriplets[i1];
            var (s11, s21, s31) = col1.ExtractTriplet();
            for (var i5 = i1; i5 < DistinctSideTriplets.Length; i5++)
            {
                var col5 = DistinctSideTriplets[i5];
                var (s15, s25, s35) = col5.ExtractTriplet();
                var cnt = mediumTripletCnt * SideTripletsCounts[i1] * SideTripletsCounts[i5];
                if (i1 != i5) cnt *= 2;
                var spCnt = mediumTriplet.TripletSpinCount() + col1.TripletSpinCount() + col5.TripletSpinCount();
                for (var i2 = 0; i2 < DistinctMediumTriplets.Length; i2++)
                {
                    var col2 = DistinctMediumTriplets[i2];
                    var (s12, s22, s32) = col2.ExtractTriplet();
                    var spinCount = spCnt + col2.TripletSpinCount();
                    for (var i4 = i2; i4 < DistinctMediumTriplets.Length; i4++)
                    {
                        var (s14, s24, s34) = DistinctMediumTriplets[i4].ExtractTriplet();
                        var line1Comb = SlotCombination.GetCombinationForLine(s11, s12, s13, s14, s15);
                        var line2Comb = SlotCombination.GetCombinationForLine(s21, s22, s23, s24, s25);
                        var line3Comb = SlotCombination.GetCombinationForLine(s31, s32, s33, s34, s35);
            
                        var line4Comb = SlotCombination.GetCombinationForLine(s11, s22, s33, s24, s15);
                        var line5Comb = SlotCombination.GetCombinationForLine(s31, s22, s13, s24, s35);
            
                        var line6Comb = SlotCombination.GetCombinationForLine(s11, s22, s23, s24, s15);
                        var line7Comb = SlotCombination.GetCombinationForLine(s31, s22, s23, s24, s35);
            
                        var line8Comb = SlotCombination.GetCombinationForLine(s11, s12, s23, s34, s35);
                        var line9Comb = SlotCombination.GetCombinationForLine(s31, s32, s23, s14, s15);

                        var combId = SlotCombination.GetCombinationId(
                            line1Comb, line2Comb, line3Comb, line4Comb,
                            line5Comb, line6Comb, line7Comb, line8Comb, 
                            line9Comb, spinCount + DistinctMediumTriplets[i4].TripletSpinCount());
                        var finalCnt = cnt * MediumTripletsCounts[i2] * MediumTripletsCounts[i4];
                        if (i2 != i4) finalCnt *= 2;
                        distrib.AddResult(combId, finalCnt);
                    }
                }
            }
        }
        results.ReturnInstance(distrib);
    }

    
}