namespace SlotMachineCalculator;

public class SampleResultInfo
{
    public decimal ExpectedValue { get; set; }
    public decimal ConfIntervalLower { get; set; }
    public decimal ConfIntervalUpper { get; set; }
    public decimal Variance { get; set; }
    public decimal TotalCases { get; set; }

    public string GetReport()
    {
        return $"{ExpectedValue:#0.0000000} [{ConfIntervalLower:#0.000}..{ConfIntervalUpper:#0.000}] {Variance:#0.00000}";
    }
}