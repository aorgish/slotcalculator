namespace SlotMachineCalculator;

public class ExactResultInfo
{
    public ulong ExpectedValueNumerator { get; set; }
    public ulong ExpectedValueDenominator { get; set; }
    public decimal ExpectedValue { get; set; }
    public decimal ExpectedValueStaticPart { get; set; }
    public decimal Variance { get; set; }
    public ulong TotalCases { get; set; }

    public string GetReport()
    {
        return $@"
Ожидаемое значение выплаты: {ExpectedValueNumerator}/{ExpectedValueDenominator}={ExpectedValue}
 в том числе: 
  - выплаты по комбинациям: {ExpectedValueStaticPart}
  - выплаты по фриспинам  : {ExpectedValue-ExpectedValueStaticPart}
Дисперсия                 : {Variance}
Среднеквадр. отклонение   : {Math.Sqrt((double)Variance)}
Всего исходов             : {TotalCases}
";
    }
}