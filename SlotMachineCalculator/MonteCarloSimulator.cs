using System.Runtime.CompilerServices;

namespace SlotMachineCalculator;

public class MonteCarloSimulator
{
    private readonly Random Rand = new();
    private int[] SlotTripletsBuffer = new int[5];

    public ResultDistribution GetRandomResultsDistribution(int roundCount)
    {
        var resultDistribution = new ResultDistribution();
        for (var i = 0; i < roundCount; i++)
        {
            var payout = GetRandomRoundResult();
            resultDistribution.AddResult(payout);
        }
        return resultDistribution;
    }

    private int GetRandomRoundResult()
    {
        var freeSpinCount = 1U;
        var payout = 0;
        while (freeSpinCount > 0)
        {
            var (slots, spinCount) = GetRandomSlotTriplets(SlotTripletsBuffer);
            var (s11, s21, s31) = slots[Consts.SLOT_COL_1].ExtractTriplet();
            var (s12, s22, s32) = slots[Consts.SLOT_COL_2].ExtractTriplet();
            var (s13, s23, s33) = slots[Consts.SLOT_COL_3].ExtractTriplet();
            var (s14, s24, s34) = slots[Consts.SLOT_COL_4].ExtractTriplet();
            var (s15, s25, s35) = slots[Consts.SLOT_COL_5].ExtractTriplet();

            var line1Comb = SlotCombination.GetCombinationForLine(s11, s12, s13, s14, s15);
            var line2Comb = SlotCombination.GetCombinationForLine(s21, s22, s23, s24, s25);
            var line3Comb = SlotCombination.GetCombinationForLine(s31, s32, s33, s34, s35);
            
            var line4Comb = SlotCombination.GetCombinationForLine(s11, s22, s33, s24, s15);
            var line5Comb = SlotCombination.GetCombinationForLine(s31, s22, s13, s24, s35);
            
            var line6Comb = SlotCombination.GetCombinationForLine(s11, s22, s23, s24, s15);
            var line7Comb = SlotCombination.GetCombinationForLine(s31, s22, s23, s24, s35);
            
            var line8Comb = SlotCombination.GetCombinationForLine(s11, s12, s23, s34, s35);
            var line9Comb = SlotCombination.GetCombinationForLine(s31, s32, s23, s14, s15);

            payout += Consts.PAYOUTS[line1Comb] +
                      Consts.PAYOUTS[line2Comb] +
                      Consts.PAYOUTS[line3Comb] +
                      Consts.PAYOUTS[line4Comb] +
                      Consts.PAYOUTS[line5Comb] +
                      Consts.PAYOUTS[line6Comb] +
                      Consts.PAYOUTS[line7Comb] +
                      Consts.PAYOUTS[line8Comb] +
                      Consts.PAYOUTS[line9Comb];

            freeSpinCount += Consts.FREE_SPIN_BONUS[spinCount];
            freeSpinCount--;
        }
        return payout;
    }


    public (int[] slot, int spinsCount) GetRandomSlotTriplets(int[] result)
    {
        var idx1 = Rand.Next(Consts.SideTriplets.Length);
        var idx2 = Rand.Next(Consts.MediumTriplets.Length);
        var idx3 = Rand.Next(Consts.MediumTriplets.Length);
        var idx4 = Rand.Next(Consts.MediumTriplets.Length);
        var idx5 = Rand.Next(Consts.SideTriplets.Length);
        
        result[Consts.SLOT_COL_1] = Consts.SideTriplets[idx1];
        result[Consts.SLOT_COL_2] = Consts.MediumTriplets[idx2];
        result[Consts.SLOT_COL_3] = Consts.MediumTriplets[idx3];
        result[Consts.SLOT_COL_4] = Consts.MediumTriplets[idx4];
        result[Consts.SLOT_COL_5] = Consts.SideTriplets[idx5];
        
        var spinsCount =
            Consts.SideTripletsSpinCount[idx1] +
            Consts.MediumTripletsSpinCount[idx2] +
            Consts.MediumTripletsSpinCount[idx3] +
            Consts.MediumTripletsSpinCount[idx4] +
            Consts.SideTripletsSpinCount[idx5];

        return (result, spinsCount);
    }
}