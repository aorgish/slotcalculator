namespace SlotMachineCalculator.Tests;

public class TripletUtilsTests
{
    [Theory]
    [InlineData("AAA", 0)]
    [InlineData("DHG", 0)]
    [InlineData("SBC", 1)]
    [InlineData("DSE", 1)]
    [InlineData("DHS", 1)]
    [InlineData("SAS", 2)]
    [InlineData("SSB", 2)]
    [InlineData("WSS", 2)]
    [InlineData("SSS", 3)]
    public void TripletSpinCountShouldReturnRightValue(string col, int spinCount)
    {
        var triplet = TripletUtils.CreateTriplet(col[0], col[1], col[2]);
        var actual = triplet.TripletSpinCount();
        Assert.Equal(spinCount, actual);
    }
    
    
}