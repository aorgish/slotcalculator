using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;

namespace SlotMachineCalculator;

public class CombinationDistribution
{
    private Dictionary<long, ulong> distribution = new(500_000);
    public IDictionary<long, ulong> GetDistribution() => distribution;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void AddResult(long combination, ulong count)
    {
        ref var cnt = ref CollectionsMarshal.GetValueRefOrAddDefault(distribution, combination, out _);
        cnt += count;
    }

    public CombinationDistribution Merge(CombinationDistribution other)
    {
        foreach (var kv in other.distribution)
        {
            AddResult(kv.Key, kv.Value);
        }
        return this;
    }

    public ExactResultInfo GetResult()
    {
        var totalCases = 0UL;
        var evConstPart = 0UL;
        var evSpinPart = 0UL;

        var sumPayouts2 = 0M;
        var sumPayouts2ForSpins = 0M;
        var sumCasesForEV2 = 0M;
        foreach (var kv in distribution)
        {
            var comb = SlotCombination.ExtractFromId(kv.Key);
            var payout = (uint) comb.GetPayout();
            var cases = kv.Value; 
            evConstPart += cases * payout;
            evSpinPart += cases * Consts.FREE_SPIN_BONUS[comb.SpinCount];
            
            sumPayouts2 += (decimal) cases * payout * payout / (Consts.PAYOUTS_MULTIPLYER * Consts.PAYOUTS_MULTIPLYER);
            sumPayouts2ForSpins +=  (decimal) cases * payout * Consts.FREE_SPIN_BONUS[comb.SpinCount] / Consts.PAYOUTS_MULTIPLYER;
            sumCasesForEV2 += (decimal) cases * Consts.FREE_SPIN_BONUS[comb.SpinCount] * (Consts.FREE_SPIN_BONUS[comb.SpinCount] - 1);
            totalCases += cases;
        }

        var evNumerator = evConstPart;
        var evDenomirator = (totalCases - evSpinPart) * Consts.PAYOUTS_MULTIPLYER;
        var expectedValue = (decimal)evNumerator / evDenomirator;
        var expectedValueStatic = (decimal)evConstPart / (totalCases * Consts.PAYOUTS_MULTIPLYER) ;

        var expectedValue2 =
            sumPayouts2
            + 2M * expectedValue * sumPayouts2ForSpins
            + expectedValue * expectedValue * sumCasesForEV2;
        expectedValue2 /= (totalCases - evSpinPart);
        
        var variance = expectedValue2 - expectedValue * expectedValue;

        return new ExactResultInfo()
        {
            ExpectedValueNumerator = evNumerator,
            ExpectedValueDenominator = evDenomirator,
            ExpectedValue = expectedValue,
            ExpectedValueStaticPart = expectedValueStatic,
            Variance = variance,
            TotalCases = totalCases
        };
    }

    public void SaveToFile(string fileName)
    {
        using var stream = File.Create(fileName);
        SaveToStream(stream);
        stream.Close();
    }
    
    public void LoadFromFile(string fileName)
    {
        using var stream = File.OpenRead(fileName);
        LoadFromStream(stream);
        stream.Close();
    }

    public void SaveToStream(Stream stream)
    {
        JsonSerializer.Serialize(stream, distribution);
    }
    
    public void LoadFromStream(Stream stream)
    {
        distribution = JsonSerializer.Deserialize<Dictionary<long, ulong>>(stream);
    }

    public string GetReport()
    {
        var sb = new StringBuilder();
        var total = distribution.Values.Aggregate(0UL, (sum, x) => sum + x);
        sb.AppendLine("Таблица появления комбинаций:");
        sb.AppendLine("(Выплата без учета фриспинов, только по комбинации)\n");
        sb.AppendLine("Комбинация             Выплата   Количество  Вероятность");
        foreach (var kv in distribution.OrderByDescending(x => x.Value))
        {
            var comb = SlotCombination.ExtractFromId(kv.Key);
            sb.AppendLine($"{comb,-23} {((decimal)comb.GetPayout())/Consts.PAYOUTS_MULTIPLYER,6:0.0} {kv.Value, 12}  {100.0M * (((decimal )kv.Value) / ((decimal) total)):00.0000}%");
        }
        sb.AppendLine("--------------------------");
        sb.AppendLine($"Всего случаев: {total}");
        return sb.ToString();
    }
}

