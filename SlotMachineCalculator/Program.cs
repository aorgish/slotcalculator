﻿using System.Diagnostics;
using SlotMachineCalculator;

var sw = new Stopwatch();
sw.Start();
var calculator = new SlotCalculator();
var combinations = calculator.Calculate();
var result = combinations.GetResult();
sw.Stop();
File.WriteAllText("combinations.txt",combinations.GetReport());
combinations.SaveToFile("combinations.json");
Console.WriteLine(result.GetReport());
Console.WriteLine($"Время расчета: {sw.Elapsed:G}"); 

Console.WriteLine();
var roundCnt = 1_000_000;
Console.WriteLine($"Проверка результатов симуляциями (по {roundCnt} раундов):");
Console.WriteLine($"Ср выплата 95% интервал  Дисперсия");

var mc = new MonteCarloSimulator();
for (var i = 0; i < 10; i++)
{
    var payoutsDistribution = mc.GetRandomResultsDistribution(roundCnt);
    Console.WriteLine(payoutsDistribution.GetResult().GetReport());
}
