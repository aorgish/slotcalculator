using System.Runtime.InteropServices;
using System.Text;
using MathNet.Numerics.Distributions;

namespace SlotMachineCalculator;

public class ResultDistribution
{
    private int[] distributionArray = new int[10_240];
    private Dictionary<int, int> distribution = new (500);

    public void AddResult(int value)
    {
        if (value < distributionArray.Length) {
            distributionArray[value]++;
        } else {
            CollectionsMarshal.GetValueRefOrAddDefault(distribution, value, out _)++;
        }
    }

    public IEnumerable<(uint payout, int count)> GetDistribution()
    {
        return distributionArray.Select((x, ind) => ((uint)ind, x)).Where(x=>x.x > 0)
            .Concat(distribution.Select(x=> ((uint)x.Key, x.Value)).OrderBy(x=>x.Item1));
    }
    
    public SampleResultInfo GetResult()
    {
        var total = 0;
        var sum = 0UL;
        var sum2 = 0UL;
        foreach (var (payout, count) in GetDistribution())
        {
            sum += (ulong) (count * payout);
            sum2 += (ulong) (count * payout * payout);
            total += count;
        }
    
        var ev = (decimal)sum / (total * Consts.PAYOUTS_MULTIPLYER);
        var ev1 = (decimal)sum / ((total-1) * Consts.PAYOUTS_MULTIPLYER);
        var var = (decimal)sum2 / ((total - 1) * Consts.PAYOUTS_MULTIPLYER * Consts.PAYOUTS_MULTIPLYER) - ev1 * ev1;
        var stdDev = Math.Sqrt((double)var);
        Normal normalDistribution = Normal.WithMeanStdDev((double) ev, stdDev);
        var confidenceLevel = 0.95;
        double criticalValue = normalDistribution.InverseCumulativeDistribution((1 + confidenceLevel) / 2);

        var epsilon = (decimal)(criticalValue * (stdDev / Math.Sqrt(total)));
        return new SampleResultInfo()
        {
            ExpectedValue = ev,
            ConfIntervalLower = ev - epsilon,
            ConfIntervalUpper = ev + epsilon,
            Variance = var,
            TotalCases = total
        };
    }
    // public (decimal ev, decimal var, int total) GetExpectedValue()
    // {
    //     var total = 0;
    //     var sum = 0UL;
    //     var sum2 = 0UL;
    //     foreach (var (payout, count) in GetDistribution())
    //     {
    //         sum += (ulong) (count * payout);
    //         sum2 += (ulong) (count * payout * payout);
    //         total += count;
    //     }
    //
    //     var ev = (decimal)sum / (total * Consts.PAYOUTS_MULTIPLYER);
    //     var ev1 = (decimal)sum / ((total-1) * Consts.PAYOUTS_MULTIPLYER);
    //     var var = (decimal)sum2 / ((total - 1) * Consts.PAYOUTS_MULTIPLYER * Consts.PAYOUTS_MULTIPLYER) - ev1 * ev1; 
    //     return (ev, var, total);
    // }

    public string GetReport()
    {
        var sb = new StringBuilder();
        var total = GetDistribution().Sum(x => x.count);
        sb.AppendLine("Distribution:");
        var lines = GetDistribution()
            .Select(x => $"{(decimal)x.payout/Consts.PAYOUTS_MULTIPLYER,5:###0.0} : {x.count,6}  {100M*x.count / total,6:0.000}%");
        var table = string.Join("\n", lines);
        sb.AppendLine(table);
        return sb.ToString();
    }
    
}