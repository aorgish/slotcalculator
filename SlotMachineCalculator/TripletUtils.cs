using System.Runtime.CompilerServices;

namespace SlotMachineCalculator;

public static class TripletUtils
{
    public static int[] GetTriplets(string signs)
    {
        var result = new int[signs.Length];
        var modulo = signs.Length;
        for (var i1 = 0; i1 < signs.Length; i1++)
        {
            var i2 = (i1 + 1) % modulo;
            var i3 = (i2 + 1) % modulo;
            var triplet = CreateTriplet(signs[i1], signs[i2], signs[i3]);
            result[i1] = triplet;
        }
        return result;
    }
    
    public static string TripletToString(this int triplet)
    {
        var (id1, id2, id3) = triplet.ExtractTriplet();
        return $"{Consts.Signs[id1]}{Consts.Signs[id2]}{Consts.Signs[id3]}";
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static (int, int, int) ExtractTriplet(this int triplet)
    {
        return (triplet & 0xF, (triplet >> 4) & 0xF, (triplet >> 8) & 0xF);
    }

    public static int CreateTriplet(int id1, int id2, int id3)
    {
        return id1 | (id2 << 4) | (id3 << 8);
    }

    public static int CreateTriplet(char id1, char id2, char id3)
    {
        return CreateTriplet(Consts.SignToId(id1), Consts.SignToId(id2), Consts.SignToId(id3));
    }

    public static int[] GetTripletsSpinCount(int[] triplets)
    {
        var result = new int[triplets.Length];
        for (var i = 0; i < triplets.Length; i++)
        {
            var triplet = triplets[i];
            result[i] = triplet.TripletSpinCount();
        }
        return result;
    }

    public static int TripletSpinCount(this int triplet)
    {
        triplet ^= Consts.SIGN_S | (Consts.SIGN_S << 4) | (Consts.SIGN_S << 8);
        return (1 >> (triplet & 0xF))
             + (1 >> ((triplet >> 4) & 0xF))
             + (1 >> ((triplet >> 8) & 0xF));
    }
}