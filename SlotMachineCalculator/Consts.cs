namespace SlotMachineCalculator;

public class Consts
{
    public const int SIGN_A = 0;
    public const int SIGN_B = 1;
    public const int SIGN_C = 2;
    public const int SIGN_D = 3;
    public const int SIGN_E = 4;
    public const int SIGN_F = 5;
    public const int SIGN_G = 6;
    public const int SIGN_H = 7;
    public const int SIGN_W = 8;
    public const int SIGN_S = 9;

    public static readonly char[] Signs = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'W', 'S' };

     public static readonly string MediumSigns = // SWHS
      "GSEDGDBWGFEAHGBCHCBFACGGEFSCAWAHEFDWCGEEGFFGHDCHGBGCHCBHDDDBFHSWHSADCHABCDSEHBDADAEFDHGCEHDSFCAAFCHCBAFDGEFDGBECBSFECWSFWFEBHCDHGGGSGBHGFAHAAAFCFHBADAWDBWEBAEAEBFCWBDWCBFHESDEEBEAG";

    public static readonly string SideSigns = MediumSigns.Replace("W","");

    public static readonly int[] MediumTriplets = TripletUtils.GetTriplets(MediumSigns);
    public static readonly int[] SideTriplets = TripletUtils.GetTriplets(SideSigns);
    
    public static readonly int[] MediumTripletsSpinCount = TripletUtils.GetTripletsSpinCount(MediumTriplets);


    public static readonly int[] SideTripletsSpinCount = TripletUtils.GetTripletsSpinCount(SideTriplets);
    

    // PAYOUT_LINE_0:  XXXXX
    //                 .....
    //                 .....
    public const int PAYOUT_LINE_0 = 0;

    // PAYOUT_LINE_1:  .....
    //                 XXXXX
    //                 .....
    public const int PAYOUT_LINE_1 = 1;

    // PAYOUT_LINE_2:  .....
    //                 .....
    //                 XXXXX
    public const int PAYOUT_LINE_2 = 2;

    // PAYOUT_LINE_3:  X...X
    //                 .X.X.
    //                 ..X..
    public const int PAYOUT_LINE_3 = 3;
    
    // PAYOUT_LINE_4:  ..X..
    //                 .X.X.
    //                 X...X
    public const int PAYOUT_LINE_4 = 4;
    
    // PAYOUT_LINE_5:  X...X
    //                 .XXX.
    //                 .....
    public const int PAYOUT_LINE_5 = 5;
    
    // PAYOUT_LINE_6:  .....
    //                 .XXX.
    //                 X...X
    public const int PAYOUT_LINE_6 = 6;
    
    // PAYOUT_LINE_7:  XX...
    //                 ..X..
    //                 ...XX
    public const int PAYOUT_LINE_7 = 7;
    
    // PAYOUT_LINE_8:  ...XX
    //                 ..X..
    //                 XX...
    public const int PAYOUT_LINE_8 = 8;

    public const int PAYOUT_LINES_COUNT = 9;

    public const int SLOT_LINE_1 =  0;
    public const int SLOT_LINE_2 =  5;
    public const int SLOT_LINE_3 = 10;
    
    public const int SLOT_COL_1 = 0;
    public const int SLOT_COL_2 = 1;
    public const int SLOT_COL_3 = 2;
    public const int SLOT_COL_4 = 3;
    public const int SLOT_COL_5 = 4;

    public static readonly uint[] FREE_SPIN_BONUS = { 0, 0, 0, 5, 10, 15, 15, 15 }; // TODO : FREE_SPIN_BONUS contain element for 7 spin count 
    
    
    public static int SignToId(char sign)
    {
        return sign switch
        {
            'A' => SIGN_A,
            'B' => SIGN_B,
            'C' => SIGN_C,
            'D' => SIGN_D,
            'E' => SIGN_E,
            'F' => SIGN_F,
            'G' => SIGN_G,
            'H' => SIGN_H,
            'W' => SIGN_W,
            'S' => SIGN_S,
            _ => throw new ArgumentException($"Invalid argument sign: '{sign}'")
        };
    }

    public const int COMBINATION_NONE =  0;
    public const int COMBINATION_A =  1;
    public const int COMBINATION_B =  4;
    public const int COMBINATION_C =  7;
    public const int COMBINATION_D = 10;
    public const int COMBINATION_E = 13;
    public const int COMBINATION_F = 16;
    public const int COMBINATION_G = 19;
    public const int COMBINATION_H = 22;

    public static readonly int[] SIGN_COMBINATION =
    {
        COMBINATION_A,
        COMBINATION_B,
        COMBINATION_C,
        COMBINATION_D,
        COMBINATION_E,
        COMBINATION_F,
        COMBINATION_G,
        COMBINATION_H
    };
    
    public const int COMBINATION_3 =  0;
    public const int COMBINATION_4 =  1;
    public const int COMBINATION_5 =  2;
    public const int PAYOUTS_BASE = 27;
    public const long PAYOUTS_BASE4 = PAYOUTS_BASE * PAYOUTS_BASE * PAYOUTS_BASE * PAYOUTS_BASE;
    
    public static uint PAYOUTS_MULTIPLYER = 10;
    public static int[] PAYOUTS =
    {
        0, 
        /*        3      4      5  */     
        /* A */   40, 200, 400,
        /* B */   30, 150, 300,
        /* C */   20, 100, 200,
        /* D */   10,  50, 100,
        /* E */    4,  20,  40,
        /* F */    3,  15,  30,
        /* G */    2,  10,  20,
        /* H */    1,   5,  10
    };
    
    public static string[] LINE_COMBINATION_NAME =
    {
        "..",
        "A3", "A4", "A5",
        "B3", "B4", "B5",
        "C3", "C4", "C5",
        "D3", "D4", "D5",
        "E3", "E4", "E5",
        "F3", "F4", "F5",
        "G3", "G4", "G5",
        "H3", "H4", "H5"
    };

}

