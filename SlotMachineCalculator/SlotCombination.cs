using System.Runtime.CompilerServices;
using System.Text;

namespace SlotMachineCalculator;

public class SlotCombination
{
    public int[] LineCombinations { get; } = new int[9];
    public int SpinCount { get; set; } // 0, 3,4,5,6,7
    public int GetPayout() => LineCombinations.Sum(x => Consts.PAYOUTS[x]);

    public override string ToString()
    {
        var sb = new StringBuilder();
        for (var i = 0; i < LineCombinations.Length; i++)
        {
            sb.Append(Consts.LINE_COMBINATION_NAME[LineCombinations[i]]);
        }
        if (SpinCount > 0) sb.Append($" S{SpinCount}");
        return sb.ToString();
    }

    public static SlotCombination ExtractFromId(long id)
    {
        var result = new SlotCombination();
        result.SpinCount = (int) (id % Consts.PAYOUTS_BASE);
        for (var i = result.LineCombinations.Length-1; i >= 0; i--)
        {
            id /= Consts.PAYOUTS_BASE;
            result.LineCombinations[i] = (int) (id % Consts.PAYOUTS_BASE); 
        }
        return result;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static long GetCombinationId(
        int l1, int l2, int l3, int l4, int l5, int l6, int l7, int l8, int l9,
        int spinCount
    )
    {
        var a1 = ((((l1 
           * Consts.PAYOUTS_BASE + l2) 
           * Consts.PAYOUTS_BASE + l3) 
           * Consts.PAYOUTS_BASE + l4) 
           * Consts.PAYOUTS_BASE + l5) 
           * Consts.PAYOUTS_BASE;
        
        var a2 = (((l6 
           * Consts.PAYOUTS_BASE + l7) 
           * Consts.PAYOUTS_BASE + l8) 
           * Consts.PAYOUTS_BASE + l9) 
           * Consts.PAYOUTS_BASE;
        
        if (spinCount<3) spinCount = 0;
        //var spins = ((0x7654_3000 >> (spinCount*4)) & 0xF); // 
        return Consts.PAYOUTS_BASE4 * a1 + a2 + spinCount;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int GetCombinationForLine(int s1, int s2, int s3, int s4, int s5)
    {
        var leftComb = Consts.COMBINATION_NONE;
        var rightComb = Consts.COMBINATION_NONE;
        
        if ((s1 <= Consts.SIGN_H) && 
            ((s2==s1) || (s2==Consts.SIGN_W)) && 
            ((s3==s1) || (s3==Consts.SIGN_W)))
        {
            var cnt = Consts.COMBINATION_3;
            if ((s4 == s1) || (s4 == Consts.SIGN_W))
            {
                if (s5 == s1) return Consts.SIGN_COMBINATION[s1] + Consts.COMBINATION_5;
                cnt = Consts.COMBINATION_4;
            }
            leftComb = Consts.SIGN_COMBINATION[s1] + cnt;
        }

        if ((s5 <= Consts.SIGN_H) && 
            ((s4==s5) || (s4==Consts.SIGN_W)) && 
            ((s3==s5) || (s3==Consts.SIGN_W)))
        {
            var cnt = ((s2 == s5) || (s2 == Consts.SIGN_W)) 
                ? Consts.COMBINATION_4
                : Consts.COMBINATION_3;
            rightComb = Consts.SIGN_COMBINATION[s5] + cnt;
        }

        return (Consts.PAYOUTS[leftComb] > Consts.PAYOUTS[rightComb])
            ? leftComb
            : rightComb;
    }

}